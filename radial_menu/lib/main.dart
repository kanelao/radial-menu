import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:fluttery_dart2/gestures.dart';
import 'package:fluttery_dart2/layout.dart';

void main() => runApp(MyApp());

//If used in the future go to pubspec.yaml and see comment in
// dependencies fluttery
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Radial Menu',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Widget _radialMenu() {
    return IconButton(
      icon: Icon(Icons.cancel),
      onPressed: () {},
    );
  }

  Widget _buildCenterMenu() {
    return AnchoredRadialMenu(
      child: _radialMenu(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: _radialMenu(),
        title: Text(''),
        actions: <Widget>[
          _radialMenu(),
        ],
      ),
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.center,
            child: _buildCenterMenu(),
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: _radialMenu(),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: _radialMenu(),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: _radialMenu(),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: _radialMenu(),
          ),
        ],
      ),
    );
  }
}

class AnchoredRadialMenu extends StatefulWidget {
  final Widget child;

  AnchoredRadialMenu({
    @required this.child,
  });

  @override
  _AnchoredRadialMenuState createState() => _AnchoredRadialMenuState();
}

class _AnchoredRadialMenuState extends State<AnchoredRadialMenu> {
  @override
  Widget build(BuildContext context) {
    return AnchoredOverlay(
      showOverlay: true,
      overlayBuilder: (BuildContext context, _, Offset anchor) {
        return RadialMenu(
          anchor: anchor,
        );
      },
      child: widget.child,
    );
  }
}

class RadialMenu extends StatefulWidget {
  final Offset anchor;
  final double radius;

  RadialMenu({@required this.anchor, this.radius = 75.0});

  @override
  _RadialMenuState createState() => _RadialMenuState();
}

class _RadialMenuState extends State<RadialMenu>
    with SingleTickerProviderStateMixin {
  static const Color expandedBubbleColor = const Color(0xFFAAAAAA);
  static const Color openBubbleColor = const Color(0xFF666666);

  RadialMenuController _menuController;

  @override
  void initState() {
    super.initState();
    _menuController = RadialMenuController(vsync: this)
      ..addListener(() => setState(() {}));

    Timer(Duration(seconds: 2), () {
      _menuController.open();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _menuController.dispose();
  }

  Widget buildCenter() {
    double scale = 1.0;
    IconData icon;
    Color bubbleColor;
    VoidCallback onPressed;

    switch (_menuController.state) {
      case RadialMenuState.closed:
        icon = Icons.menu;
        bubbleColor = openBubbleColor;
        scale = 0.0;
        break;
      case RadialMenuState.closing:
        icon = Icons.menu;
        bubbleColor = openBubbleColor;
        scale = 1.0 - _menuController.progress;
        break;
      case RadialMenuState.opening:
        icon = Icons.menu;
        bubbleColor = openBubbleColor;
        scale = _menuController.progress;
        break;
      case RadialMenuState.open:
        icon = Icons.menu;
        bubbleColor = openBubbleColor;
        scale = 1.0;
        onPressed = () {
          _menuController.expand();
        };
        break;
      case RadialMenuState.expanded:
        icon = Icons.clear;
        bubbleColor = expandedBubbleColor;
        scale = 1.0;
        onPressed = () {
          _menuController.collapse();
        };
        break;
      default:
        icon = Icons.clear;
        bubbleColor = expandedBubbleColor;
        scale = 1.0;
        break;
    }
    return CenterAbout(
      position: widget.anchor,
      child: Transform(
        transform: Matrix4.identity()..scale(scale, scale),
        alignment: Alignment.center,
        child: IconBubble(
          diameter: 50,
          icon: icon,
          iconColor: Colors.black,
          bubbleColor: bubbleColor,
          onPressed: onPressed,
        ),
      ),
    );
  }

  Widget buildRadialBubble({
    IconData icon,
    Color iconColor,
    Color bubbleColor,
    double angle,
  }) {
    if (_menuController.state == RadialMenuState.closed ||
        _menuController.state == RadialMenuState.closing ||
        _menuController.state == RadialMenuState.open ||
        _menuController.state == RadialMenuState.opening ||
        _menuController.state == RadialMenuState.dissipating)
      return Container();

      double radius = widget.radius;
      double scale = 1.0;
      if(_menuController.state == RadialMenuState.expanding){
        radius = widget.radius*_menuController.progress;
        scale = _menuController.progress;
      } else if(_menuController.state == RadialMenuState.collapsing){
        radius = widget.radius*(1-_menuController.progress);
        scale = 1-_menuController.progress;
      }

    return PolarPosition(
      origin: widget.anchor,
      coord: PolarCoord(angle, radius),
      child: Transform(
        transform: Matrix4.identity()..scale(scale,scale),
        alignment: Alignment.center,
              child: IconBubble(
          diameter: 50,
          icon: icon,
          iconColor: iconColor,
          bubbleColor: bubbleColor,
          onPressed: (){
            _menuController.activate("todo");
          }),
        ),
    );
  }

  Widget buildActivation() {
      if (_menuController.state != RadialMenuState.activating &&
        _menuController.state == RadialMenuState.dissipating )
        return Container();

    return CenterAbout(
      position: widget.anchor,
      child: CustomPaint(
        painter: ActivationPainter(
          radius: widget.radius,
          color: Colors.blue,
          thickness: 50.0,
          startAngle: -pi / 2,
          endAngle: -pi / 2,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        buildCenter(),
        buildRadialBubble(
          iconColor: Colors.white,
          icon: Icons.home,
          angle: -pi / 2,
          bubbleColor: Colors.blue,
        ),
        buildRadialBubble(
          iconColor: Colors.white,
          icon: Icons.search,
          angle: -pi / 2 + (1 * 2 * pi / 5),
          bubbleColor: Colors.green,
        ),
        buildRadialBubble(
          iconColor: Colors.white,
          icon: Icons.alarm,
          angle: -pi / 2 + (2 * 2 * pi / 5),
          bubbleColor: Colors.red,
        ),
        buildRadialBubble(
          iconColor: Colors.white,
          icon: Icons.settings,
          angle: -pi / 2 + (3 * 2 * pi / 5),
          bubbleColor: Colors.purple,
        ),
        buildRadialBubble(
          iconColor: Colors.white,
          icon: Icons.location_on,
          angle: -pi / 2 + (4 * 2 * pi / 5),
          bubbleColor: Colors.orange,
        ),
        buildActivation(),
      ],
    );
  }
}

class ActivationPainter extends CustomPainter {
  final double radius;
  final double thickness;
  final Color color;
  final double startAngle;
  final double endAngle;
  final Paint activationPaint;

  ActivationPainter({
    @required this.radius,
    @required this.thickness,
    @required this.color,
    @required this.startAngle,
    @required this.endAngle,
  }) : activationPaint = Paint()
          ..color = color
          ..strokeWidth = thickness
          ..style = PaintingStyle.stroke
          ..strokeCap = StrokeCap.round;

  @override
  void paint(Canvas canvas, Size size) {
    canvas.drawArc(Rect.fromLTRB(-radius, -radius, radius, radius), startAngle,
        endAngle - startAngle, false, activationPaint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class IconBubble extends StatelessWidget {
  final IconData icon;
  final double diameter;
  final Color iconColor;
  final Color bubbleColor;
  final VoidCallback onPressed;

  IconBubble({
    @required this.icon,
    @required this.diameter,
    @required this.iconColor,
    @required this.bubbleColor,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: Container(
        width: diameter,
        height: diameter,
        decoration: BoxDecoration(shape: BoxShape.circle, color: bubbleColor),
        child: Icon(
          icon,
          color: iconColor,
        ),
      ),
    );
  }
}

class PolarPosition extends StatelessWidget {
  final Offset origin;
  final PolarCoord coord;
  final Widget child;

  PolarPosition(
      {@required this.origin, @required this.coord, @required this.child});

  @override
  Widget build(BuildContext context) {
    final radialPosition = Offset(origin.dx + (cos(coord.angle) * coord.radius),
        origin.dy + (sin(coord.angle) * coord.radius));

    return CenterAbout(
      position: radialPosition,
      child: child,
    );
  }
}

class RadialMenuController extends ChangeNotifier {
  final AnimationController _progress;
  RadialMenuState _state = RadialMenuState.closed;

  RadialMenuController({
    @required TickerProvider vsync,
  }) : _progress = AnimationController(vsync: vsync) {
    _progress
      ..addListener(_onProgressUpdate)
      ..addStatusListener((AnimationStatus status) {
        if (status == AnimationStatus.completed) _onTransitionCompleted();
      });
  }

  void _onProgressUpdate() {
    notifyListeners();
  }

  void _onTransitionCompleted() {
    switch (_state) {
      case RadialMenuState.closing:
        _state = RadialMenuState.closed;
        break;
      case RadialMenuState.opening:
        _state = RadialMenuState.open;
        break;
      case RadialMenuState.expanding:
        _state = RadialMenuState.expanded;
        break;
      case RadialMenuState.collapsing:
        _state = RadialMenuState.open;
        break;
      case RadialMenuState.activating:
        _state = RadialMenuState.dissipating;
        _progress.duration = Duration(milliseconds: 250);
        _progress.forward(from: 0.0);
        break;
      case RadialMenuState.dissipating:
        _state = RadialMenuState.open;
        break;
      case RadialMenuState.closed:
      case RadialMenuState.expanded:
      case RadialMenuState.open:
        throw Exception('Invalid state during a transition $_state');
        break;
    }
    notifyListeners();
  }

  RadialMenuState get state => _state;
  double get progress => _progress.value;

  void open() {
    if (state != RadialMenuState.closed) return;

    _state = RadialMenuState.opening;
    _progress.duration = Duration(milliseconds: 250);
    _progress.forward(from: 0.0);
    notifyListeners();
  }

  void close() {
    if (state != RadialMenuState.open) return;

    _state = RadialMenuState.closing;
    _progress.duration = Duration(milliseconds: 250);
    _progress.forward(from: 0.0);
    notifyListeners();
  }

  void expand() {
    if (state != RadialMenuState.open) return;

    _state = RadialMenuState.expanding;
    _progress.duration = Duration(milliseconds: 150);
    _progress.forward(from: 0.0);
    notifyListeners();
  }

  void collapse() {
    if (state != RadialMenuState.expanded) return;

    _state = RadialMenuState.collapsing;
    _progress.duration = Duration(milliseconds: 150);
    _progress.forward(from: 0.0);
    notifyListeners();
  }

  void activate(String menuItemId) {
    if (state != RadialMenuState.expanded) return;

    _state = RadialMenuState.activating;
    _progress.duration = Duration(milliseconds: 500);
    _progress.forward(from: 0.0);
    notifyListeners();
  }
}

enum RadialMenuState {
  closed,
  closing,
  opening,
  open,
  expanding,
  collapsing,
  expanded,
  activating,
  dissipating,
}
